FROM debian:testing-slim

ARG DEBIAN_FRONTEND=noninteractive

# Install build tools
RUN apt-get update && apt-get install -y --no-install-recommends git make texinfo bison flex \
  gettext dwarves meson python3-sphinx-rtd-theme libudev-dev pkg-config clang doxygen clang \
  emscripten llvm

# Install required libraries
RUN apt-get install -y --no-install-recommends gcc-multilib g++-multilib libgmp-dev \
  libmpfr-dev libmpc-dev libssl-dev libelf-dev libsqlite3-dev libasound2-dev libjpeg-dev \
  libjson-c-dev libqt5opengl5-dev libsdl2-dev libbpf-dev libgtk-3-dev libc++-dev libc++1 \
  libc++abi-dev libc++abi1 libclang-dev libclang1 liblldb-dev libllvm-ocaml-dev libomp-dev libomp5

# Install additional utilities
RUN apt-get install -y --no-install-recommends dvipng rsync wget clang-tidy clang-tools clangd \
  xz-utils clang-format python3-argcomplete bc python3-clang lld lldb abi-dumper abi-compliance-checker

# Install packages required for virtualization
RUN apt-get install -y --no-install-recommends qemu-system-x86  virtme-ng

RUN apt-get clean && rm -rf /var/lib/apt/lists/*


# Move .git/refs/heads/master into the container in order to read the git hash of the current version of the build-scripts
COPY ./cross.sh ./funcs.sh ./prepare_build.sh ./build.sh ./configs ./gitmenu.sh ./pahole*\
  ./prepare_kernel.sh ./upload.sh ./virtme-test.sh parselog.pl .git/refs/heads/master /build-scripts/
RUN git config --global --add safe.directory /build-scripts/media-git

WORKDIR /build-scripts
