#!/bin/bash

git clone git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux-2.6.git build-data/media-git
cd build-data/media-git
git remote add media_tree git://linuxtv.org/media_tree.git
git remote add media_stage git://linuxtv.org/media_stage.git
cd ../..
