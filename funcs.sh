#!/bin/bash

top=$PWD
build_data=$PWD/build-data
cross_data=$PWD/cross
log_data=$PWD/logs

if [ ! -f env.sh ]; then
	echo The file env.sh was not found in the current directory $top.
	exit 1
fi

. ./env.sh

if [ "$use_ccache" != "0" ]; then
	if [ -x /usr/bin/ccache ]; then
		use_ccache=1
	else
		use_ccache=0
	fi
fi

function setup_arch()
{
	a=$1
	cross=$a-linux

	case $a in
	i686)
		a=i386
		;;
	x86_64) 
		;;
	powerpc64)
		a=powerpc
		;;
	arm)
		cross=arm-linux-gnueabi
		;;
	arm64)
		a=arm64
		cross=aarch64-linux-gnu
		;;
	esac
	export PATH=$cross_data/bin/$cross/bin:$PATH
	if [ $use_ccache = "1" ]; then
		export PATH=$cross_data/ccache:$PATH
	fi
	opts="ARCH=$a CROSS_COMPILE=$cross- -Oline"
}

function setup_config()
{
	arch=$1
	conf=$2

	cd $build_data/media-git
	rm -rf $build_data/trees/$conf/media-git
	mkdir $build_data/trees/$conf/media-git
	make mrproper
	make $opts O=$build_data/trees/$conf/media-git allyesconfig
	cd $build_data/trees/$conf/media-git
	if [ -f $top/configs/$conf.config ]; then
		cp $top/configs/$conf.config .config
	fi
	if [ $arch = x86_64 -o $arch = i686 ]; then
		perl -p -i -e 's/CONFIG_STAGING_EXCLUDE_BUILD=y/CONFIG_STAGING_EXCLUDE_BUILD=n/' .config
		perl -p -i -e 's/CONFIG_KCOV=y/CONFIG_KCOV=n/' .config
		perl -p -i -e 's/CONFIG_WERROR=y/CONFIG_WERROR=n/' .config
		perl -p -i -e 's/CONFIG_TOUCHSCREEN_(.*)=y/CONFIG_TOUCHSCREEN_\1=n/' .config
		perl -p -i -e 's/CONFIG_TOUCHSCREEN_SUR40=n/CONFIG_TOUCHSCREEN_SUR40=y/' .config
		( yes n | make $opts olddefconfig >/dev/null 2>&1 & ) ; sleep 5; kill `pidof yes` 2>/dev/null
		( yes | make $opts olddefconfig >/dev/null 2>&1 & ) ; sleep 5; kill `pidof yes` 2>/dev/null
	fi
	cd $build_data/media-git
	make $opts O=$build_data/trees/$conf/media-git prepare
}

function strip_top()
{
	sed "s;$build_data/media-git/;;g"
}

function build_smatch()
{
	if [ ! -d $build_data/smatch ]; then
		echo '`smatch` folder missing, run prepare_build.sh first'
		exit 1
	fi
	cd $build_data/smatch
	git remote update
	git reset --hard $1
	make -j HAVE_LLVM=no
	cd $top
	return 0
}

function setup()
{
	mkdir -p $build_data
	if [ ! -d $build_data/smatch ] || [ ! -d $build_data/v4l-utils ] || [ ! -d $build_data/edid-decode ]; then
		$top/prepare_build.sh $build_data
	fi
	mkdir -p $build_data/trees
	cd $build_data/trees
	mkdir -p $architectures

	build_smatch origin/master

	exit 0
}

function print_usage()
{
	echo 'Usage: build.sh [-patches N] [-test] [-misc] [-sparse] [-smatch]'
	echo '                [-no-pm] [-no-pm-sleep] [-no-of] [-no-acpi] [-no-debug-fs]'
	echo '                [-kerneldoc] [-spec] [-utils] [-sequential] [-cpus N]'
	echo '                [-virtme-64] [-virtme-64-arg ARG] [-virtme-32] [-virtme-32-arg ARG]'
	echo '                [-virtme] [-virtme-quick]'
	#echo '                [-daily] [-daily-test]'
	echo '                arch|all|none [branch [remote]]'
	echo '       build.sh [clean|setup]'
	echo
	echo 'clean:  delete all logs, history and repositories.'
	echo 'ccache-clean: delete trees/*/ccache-git.'
	echo 'setup:  does the same as clean and then sets up all the repositories'
	echo '        from scratch.'
	echo
	echo '-test: enables all options except -utils and -virtme.'
	echo '-misc: enables several miscellaneous tests.'
	echo '-patches N: add the last N patches from the branch one by one and check if'
	echo '            it builds on x86_64 to prevent compile breakage.'
	echo '            Also run checkpatch over each patch. By default N=0.'
	echo '-check-patches N: same as -patches, but skip the compilation.'
	echo '            So just run checkpatch over each patch. By default N=0.'
	echo '-no-pm: turns on the x86_64 CONFIG_PM=n build for the test run.'
	echo '-no-pm-sleep: turns on the x86_64 CONFIG_PM_SLEEP=n build for the test run.'
	echo '-no-of: turns on the x86_64 CONFIG_OF=n build for the test run.'
	echo '-no-acpi: turns on the x86_64 CONFIG_ACPI=n build for the test run.'
	echo '-no-debug-fs turns on the x86_64 CONFIG_DEBUG_FS=n build for the test run.'
	echo '-sparse: turns on the x86_64 sparse build for the test run.'
	echo '-smatch: turns on the x86_64 smatch build for the test run.'
	echo '-kerneldoc: checks kerneldoc of all media headers.'
	echo '-spec: build media specification docs.'
	echo '-utils: build v4l-utils and edid-decode.'
	echo '-virtme: shorthand for -virtme-64 -virtme-32.'
	echo '-virtme-64: build a virtme kernel and run the regression tests in 64 bit.'
	echo '-virtme-64-arg: the argument to the test-media script (default is 'mc').'
	echo '-virtme-32: build a virtme kernel and run the regression tests in 32 bit.'
	echo '-virtme-32-arg: the argument to the 32-bit test of test-media script (default is 'mc').'
	echo '-virtme-quick: skip building the virtme kernel, just run the regression tests.'
	echo '-virtme-utils-path: where to find the v4l-utils utilities, by default use what -utils built.'
	echo '-virtme-test-media: the test-media script to run. By default use v4l-utils/contrib/test/test-media.'
	echo '-cpus N: use -jN for make when building git trees. If N is 0, then use -j only.'
	echo '         The parallel builds will use -j N/4. By default N is the number of'
	echo '         CPUs reported in /proc/cpuinfo.'
	echo '-sequential: build all architectures/configs sequentially instead of in parallel.'
	#echo '-daily: builds against all the specified architectures, builds all'
	#echo '        utilities, the special configs and the sparse/smatch builds and mails'
	#echo '        the results to the mailinglist.'
	#echo '-daily-test: does the same as -daily, except it sends the results to the builder only,'
	#echo '        not to the mailinglist.'
	echo
	echo 'arch|all|none: either specify a specific arch, do them all or do nothing.'
	echo
	echo 'Available architectures: '$architectures
	echo
	echo "branch: the branch to use. Default is 'master' with 'media_stage' as the remote."
	echo "remote: the remote to use. Default is 'main'."
}

function set_doc_headers()
{
	DOCHDRS=include/uapi/linux/lirc.h
	DOCHDRS="$DOCHDRS include/uapi/linux/videodev2.h"
	DOCHDRS="$DOCHDRS include/uapi/linux/ivtv*.h"
	DOCHDRS="$DOCHDRS include/uapi/linux/max2175.h"
	DOCHDRS="$DOCHDRS include/uapi/linux/media*.h"
	DOCHDRS="$DOCHDRS include/uapi/linux/v4l2*.h"
	DOCHDRS="$DOCHDRS include/uapi/linux/uvcvideo.h"
	DOCHDRS="$DOCHDRS include/uapi/linux/xilinx-v4l2-controls.h"
	DOCHDRS="$DOCHDRS include/uapi/linux/ccs.h"
	DOCHDRS="$DOCHDRS include/uapi/linux/smiapp.h"
	DOCHDRS="$DOCHDRS include/uapi/linux/cec*.h"
	DOCHDRS="$DOCHDRS "`find include/media`
	DOCHDRS="$DOCHDRS "`find include/uapi/linux/dvb`
	DOCHDRS="$DOCHDRS "`find drivers/media/|grep \\\.h$`
	DOCHDRS="$DOCHDRS "`find drivers/staging/media/|grep -v atomisp|grep \\\.h$`
}

architectures="arm arm64 powerpc64 i686 x86_64"

if [ -z "$CPUS" ]; then
	CPUS=`cat /proc/cpuinfo | grep -i processor | wc -l`
fi
