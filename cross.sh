#!/bin/bash

. ./funcs.sh || exit 1

CROSS=$1
CPUS=`cat /proc/cpuinfo|grep ^processor|wc -l`
archs="$architectures"
gcc_vers=13.2.0
binutils_vers=2.41

if [ -z "$1" ]; then
	echo "Usage: cross.sh arch|all|clean|setup"
	echo
	echo "clean: delete the cross directory"
	echo "build-clean: delete the temporary build directories in the cross directory"
	echo "setup: download gcc/binutils and set up the build environment"
	echo
	echo "arch|all: build a cross-compilation environment for the specified"
	echo "       architecture or all of them."
	echo
	echo "available architectures: $archs"
	exit 0
fi

if [ "$1" = "clean" ]; then
	rm -rf $cross_data/*
	exit 0
fi

if [ "$1" = "build-clean" ]; then
	rm -rf $cross_data/gcc* $cross_data/binutils*
	exit 0
fi

if [ "$1" = "setup" ]; then
	mkdir -p $cross_data/packages
	mkdir -p $cross_data/bin
	mkdir -p $cross_data/ccache
	cd $cross_data/packages
	wget -c ftp://ftp.gnu.org/gnu/binutils/binutils-$binutils_vers.tar.xz
	wget -c ftp://ftp.gnu.org/gnu/gcc/gcc-$gcc_vers/gcc-$gcc_vers.tar.xz
	exit 0
fi

cd $cross_data

if [ ! -d binutils-$binutils_vers ]; then
	tar xaf $cross_data/packages/binutils-$binutils_vers.tar.xz
fi
if [ ! -d gcc-$gcc_vers ]; then
	tar xaf $cross_data/packages/gcc-$gcc_vers.tar.xz
fi

cd $top

if [ "$1" = "all" ]; then
	for i in $archs
	do
		$top/cross.sh $i
	done
	exit 0
fi

cd $cross_data

echo Cross compile for: $CROSS

newlib=--with-newlib
if [ $CROSS = "arm" ]; then
	CROSS="arm-linux-gnueabi"
elif [ $CROSS = "arm64" ]; then
	CROSS="aarch64-linux-gnu"
else
	CROSS="$CROSS-linux"
fi

rm -rf gcc-build &&
mkdir gcc-build &&
cd $cross_data/gcc-build &&
$cross_data/gcc-$gcc_vers*/configure --target=$CROSS --prefix=$cross_data/bin/$CROSS --disable-shared --disable-threads --enable-languages=c $newlib &&
make all-gcc -j$CPUS &&
make install-gcc &&
cp $cross_data/gcc-$gcc_vers/gcc/cp/cp-trait.def $cross_data/bin/$CROSS/lib/gcc/$CROSS/$gcc_vers/plugin/include/cp/cp-trait.def &&
cd $cross_data &&
rm -rf binutils-build &&
mkdir binutils-build &&
cd binutils-build &&
$cross_data/binutils-$binutils_vers/configure --target=$CROSS --prefix=$cross_data/bin/$CROSS --disable-werror &&
make -j$CPUS &&
make install &&
cd $top &&
ln -sf /usr/bin/ccache $cross_data/ccache/$CROSS-gcc
