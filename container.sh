#!/bin/bash

IMAGE_TAG="$(date +"%Y-%m-%d")_media_build"

case "$1" in
  run)
    if [ ! "$2" ]; then
      echo "Usage: $0 run IMAGE_TAG"
      exit 1
    fi
    mkdir -p build-data cross logs
    podman run -it \
    --mount type=bind,source="$(pwd)"/custom_data/env.sh,destination=/build-scripts/env.sh,readonly \
    --mount type=bind,source="$(pwd)"/build-data,destination=/build-scripts/build-data \
    --mount type=bind,source="$(pwd)"/cross,destination=/build-scripts/cross \
    --mount type=bind,source="$(pwd)"/logs,destination=/build-scripts/logs \
    "$2" \
    /bin/bash
    ;;
  build)
    mkdir -p custom_data
    cat > custom_data/env.sh << EOF
myrepo=git://linuxtv.org/hverkuil/media_tree.git
name="Example Name"
email="example@mail.com"
EOF
    if [ ! -d build-data/media-git ]; then
      ./prepare_kernel.sh
    fi
    podman build -t $IMAGE_TAG --progress=plain . 2>&1 | tee build.log
    ;;
  *)
    echo "Usage: $0 {run IMAGE_TAG|build}"
    exit 1
esac

