#!/bin/bash

cd $1
git clone git://linuxtv.org/v4l-utils.git
git clone git://linuxtv.org/edid-decode.git
if [ ! -d smatch ]; then
  git clone git://repo.or.cz/smatch.git
fi
