#!/bin/bash

apt-get update

# Install build tools
apt-get install -y --no-install-recommends git make texinfo bison flex \
  gettext dwarves meson python3-sphinx-rtd-theme libudev-dev pkg-config clang doxygen clang \
  emscripten llvm zstd

# Install required libraries
apt-get install -y --no-install-recommends gcc-multilib g++-multilib libgmp-dev \
  libmpfr-dev libmpc-dev libssl-dev libelf-dev libsqlite3-dev libasound2-dev libjpeg-dev \
  libjson-c-dev libqt5opengl5-dev libsdl2-dev libbpf-dev libgtk-3-dev libc++-dev libc++1 \
  libc++abi-dev libc++abi1 libclang-dev libclang1 liblldb-dev libllvm-ocaml-dev libomp-dev libomp5

# Install additional utilities
apt-get install -y --no-install-recommends dvipng rsync wget clang-tidy clang-tools clangd \
  xz-utils clang-format python3-argcomplete bc python3-clang lld lldb abi-dumper abi-compliance-checker \
  python3-junitparser b4 graphviz

# Install packages required for virtualization
apt-get install -y --no-install-recommends kmod qemu-system-x86 udev iproute2 libv4l-0

# Install virtme-ng
git clone https://github.com/arighi/virtme-ng.git
pushd virtme-ng
git fetch --tags
git checkout v1.18
./setup.py install --prefix=/usr
popd
rm -fr virtme-ng

# Fetch main linux repositories
source ci/docker-repos.sh

mkdir -p /opt/build/media-git
git init /opt/build/media-git

for repo in ${GIT_REPOS}; do
	REMOTE="$(echo $repo | cut -d, -f1)"
	BRANCH="$(echo $repo | cut -d, -f2)"
	NAME="$(echo $repo | cut -d, -f3)"

	echo "Adding branch ${BRANCH} from git repo ${REMOTE} as ${NAME}"
	git -C /opt/build/media-git remote add ${NAME} ${REMOTE}
	git -C /opt/build/media-git fetch --depth=1 ${NAME} ${BRANCH}
done

cat <<EOF >> env.sh 
#!/bin/bash
cross_data=/opt/cross
build_data=/opt/build

myrepo=git://linuxtv.org/hverkuil/media_tree.git
mylocalrepo=/opt/linux

use_ccache=1

name="${GITLAB_USER_NAME}"
email="${GITLAB_USER_EMAIL}"
EOF

cp env.sh /opt/env.sh

./cross.sh setup
./cross.sh all
./build.sh setup

