#!/usr/bin/python3

import sys
from junitparser import TestCase, TestSuite, JUnitXml, Failure, Element

state = "results"
blank_lines = 0
cases = []


def get_result(result_str: str) -> Element:
    match result_str:
        case "WARNINGS":
            return Failure('Warnings detected', 'WARNING')
        case "ERRORS":
            return Failure('Errors detected', 'ERROR')
        case "OK":
            return None
        case _:
            raise Exception("Not a test result")

# Parse the summary file. The format just text with lines fomatted as:
# <field>: <value>
#
# The first part of the summary is made of some information values like
# timestamps, tools versions, tested version,...
# Then a blank line, then test results.
# Test results are formatted as:
# <testname>: <result>
# [blank line]
# [result details]
# [blank line]
# ...
# <result> can be either: OK, WARNINGS or ERRORS.
# If result is not OK. the [result details] are available on the
# following lines, between blank lines.
# 
# This program parses that:
# - First part of the summary is the intro,
# - Then results,
# - If results is not OK -> parse_results until 2 blank lines are found.

f = open(sys.argv[1], "r")
for x in f:
    if x.strip() and state == "results":
        info = x.split(":")
        case = TestCase(info[0])
        try:
            case.result = [get_result(info[1].strip())]
        except:
            continue
        if len(case.result) and case.result[0]:
            case.result[0].text = ""
            state = "parse_results"
            blank_lines = 0
        cases.append(case)
    elif state == "parse_results" and not x.strip():
        blank_lines += 1
        if blank_lines == 2:
            state = "results"
    elif state == "parse_results":
        cases[-1].result[0].text += x

suite = TestSuite('suite1')
suite.add_testcases(cases)

# Add suite to JunitXml
xml = JUnitXml()
xml.add_testsuite(suite)
xml.write('junit.xml')
