
GIT_REPOS="\
	https://git.linuxtv.org/media_tree.git,master,media_tree \
	https://git.linuxtv.org/media_stage.git,master,media_stage \
	git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git,master,torvalds \
	https://gitlab.collabora.com/sebastianfricke/linux.git,codec_staging,codec_staging \
"

