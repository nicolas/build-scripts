# Automated test run

This folder contains scripts that are used to all tests in a Gitlab pipeline.

# Test commit

Each manual run of the pipeline prompts 3 variables. 2 are mandatory:
- `TEST_GIT_REPOSITORY`: The git repository to be cloned and tested.
- `TEST_GIT_BRANCH`: The branch to be tested.

Only setting these two will run the pipeline on the latest commit of the
specified branch.

Additionally, `TEST_PATCHSET_ID` can be set to a mailing list patchset id.
If set, `b4` will be used to apply the whole patchset on top of the branch
given in `TEST_GIT_BRANCH`.

# Pipeline stages

The pipeline runs in 3 stages.
This is done to balance the load on the correct gitlab runners.

## docker stage

This stage is used to build the FDO-based docker image in which the next stages
will run.
The image is based on Debian bookworm (11) in which extra packages are installed
and the cross compilers and tools are built so that they are not rebuilt for each
pipeline.
The `env.sh` file is also setup at this stage.
See the `ci/docker_prepare.sh` file for more information on what is added to the
image.

The container image is stored on the Gitlab repository's container registry.
It is only built when the value of `DEBIAN_TAG` is modified in the
`.gitlab-image-tags.yml` file.
That is only necessary when the Debian base image needs to be updated or when
`ci/docker_prepare.sh` is changed.

## build

The build stage is used to clone the git repository, optionally get the b4 patch
set and build the virtme kernel and modules.

The linux sources and the built kernel are then packed and used as artifacts to
be used by the next stage.
The virtme kernel needs to be built at the stage because the actual virtme tests
are run on a kvm gitlab runner which is not powerful enough to build the kernel
in a timely manner.

## tests

On this stage, the actual tests are run.
This will use the `build.sh` script to run the tests, log everything and generate
a text summary.

The text summary is then parsed (by `ci/summary_to_junit.py`) to transform it
into a junit XML file that can be shown in the Gitlab web interface.

### test-all

This job is responsible of running all tests except for the virtme ones.
It basically runs `build.sh -test all` that builds linux and runs different tools
like `smatch` and logs build warnings or errors.

### test-virtme

Only run the virtme tests.
It uses the `-virtme-quick` argument to skip building linux on the KVM runner and
uses the built kernel from the artifacts of the `build` stage.

# Limitations

## Cloning

For each pipeline, the linux repository needs to be cloned.
The `--depth=1` and `--branch=$TEST_GIT_BRANCH` arguments are used with git to
limit the data to be downloaded but that could still be too much.

If this become a problem, a solution could be to use a cache, but it would need
to be refreshed sometimes to keep it up to date.

## JUnit and warning

The tests text summary gives 3 types of results:
- OK
- WARNING
- ERROR

Unfortunately, junit has no support for warning, only success or fail.
There are ways around that with junit but none are supported by Gitlab so,
warnings are treated as errors for now in the web interface.

## Bisect tests

The `build.sh` script has support for `bisect` tests: it applies patches one by
one and checks that linux builds each time.
This is currently not supported.

# Base sripts adaptations

Some adaptations have been done to the upstream scripts.
These are necessary to help building with CI and should not change the normal
behavior of upstream versions.

These changes are in separate commits, so ultimately, they could be upstreamed
to Hans' repo, depending on what the future holds for the different
repositories.

