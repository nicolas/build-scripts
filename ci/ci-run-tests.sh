#!/bin/bash

set -x

source ci/linux-common.sh

cp /opt/env.sh ./env.sh

echo "--- env.sh: ---"
cat env.sh
echo "---------------"

if [ "${1}" = "virtme" ]; then
	mkdir -p /opt/build/trees
	tar -xf virtme.tar.zstd -C /
	./build.sh -virtme -virtme-quick none
else
	# If the remote is not present, get it from the artifact
	if [ -z "${TEST_REMOTE}" ]; then
		TEST_REMOTE="test"
		git -C /opt/build/media-git remote add "test" "${GIT_REPO}"

		tar -xf media-git.tar.zstd -C /
	else
		# Fetch the latest version of the repository
		git -C /opt/build/media-git fetch --depth=1 ${TEST_REMOTE} ${GIT_BRANCH}
		git -C /opt/build/media-git checkout ${TEST_REMOTE}/${GIT_BRANCH}
	fi

	./build.sh -test all
fi

./ci/summary_to_junit.py logs/logs/summary tests.xml
